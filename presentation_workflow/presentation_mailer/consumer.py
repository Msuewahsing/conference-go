import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host='conference-go-rabbitmq-1')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

try:
    def process_approval(ch, method, properties, body):
        presentation = json.loads(body)

        send_mail(
            'presentation_approved!!',
            message=f"The following presentation was approved:{presentation['title']}",
            from_email='foo@bar.com',
            recipient_list=[presentation['presenter_email']],
            fail_silently=False
        )

        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()

    def process_rejection(ch, method, properties, body):
        print(body)
        presentation = json.loads(body)

        send_mail(
            'presentation rejected!!',
            message=f"The following presentation was rejected:{presentation['title']}",
            from_email='foo@bar.com',
            recipient_list=[presentation['presenter_email']],
            fail_silently=False
        )

        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

except AMQPConnectionError:
    time.sleep(2.0)
