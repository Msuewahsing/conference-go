from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


def UpdateAccountVo(ch, method, properties, body):
    print("function called")
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["updated"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            defaults={
                "email": email,
                "first_name": first_name,
                "last_name": last_name,
                "is_active": is_active,
                "updated": updated,
            },
        )
    else:
        try:
            AccountVO.objects.filter(email=email).delete()
        except AccountVO.DoesNotExist:
            pass


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        que_name_random = channel.queue_declare(queue="", exclusive=True)
        que_name = que_name_random.method.queue
        channel.queue_bind(exchange="account_info", queue=que_name)
        channel.basic_consume(
            queue=que_name,
            auto_ack=True,
            on_message_callback=UpdateAccountVo,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("could not connect to rabbitmq ")
        time.sleep(5.0)
