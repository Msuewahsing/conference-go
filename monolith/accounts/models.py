from django.contrib.auth.models import AbstractUser, User
from django.db import models
from datetime import datetime



class User(AbstractUser):
    email = models.EmailField(unique=True)


