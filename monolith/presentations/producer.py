import pika
import json

def send_presentation_to_queue(presentation, queue_name):
    parameters = pika.ConnectionParameters(host="conference-go-rabbitmq-1")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    presentation_dict = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title

    }
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=json.dumps(presentation_dict),
    )
    connection.close()


def send_presentation_approval(presentation):
    send_presentation_to_queue(presentation, 'presentation_approvals')



def send_presentation_rejection(presentation):
    send_presentation_to_queue(presentation, 'presentation_rejections')
